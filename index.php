<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("inc/config.php");
		require_once_classes(array("CSession", "CMysqlManager", "CBgComponentManager", "CAccountManager", "CPlayerManager", "CPopupComponentManager"));
		
		$session = new CSession();
		$account_info = $session->getLoginData();
			
		if(!isset($account_info)) {
			moveToSpecificPage("/login/login.php");
			exit;
		}
		
		$mysql_manager = new CMysqlManager();
		
		$bg_component_manager = new CBgComponentManager($mysql_manager->getDb());
		$player_manager = new CPlayerManager($mysql_manager->getDb());
		$popup_component_manager = new CPopupComponentManager($mysql_manager->getDb());
		
		$player_list = $player_manager->getPlayerList();
	} catch (Exception $e) {
		echo "<pre>";
		print_r($e->getMessage());
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<title><?php echo CONF_SITE_TITLE; ?></title>
		
		<!-- Bootstrap 3.3.6 -->
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<!-- Font Awesome -->
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<!-- Ionicons -->
  		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/ionicons.min.css">
		<!-- Theme style -->
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/AdminLTE.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/skins/_all-skins.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/loading.css">
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time(); ?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/fastclick.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/adminLTE.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/md5.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.form.min.js"></script>
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/player_manager.js?<?php echo time(); ?>"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/component_manager.js?<?php echo time(); ?>"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/main.js?<?php echo time(); ?>"></script>
	
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="loading">Loading</div>
		<div class="wrapper">
			<header class="main-header">
				<a href="/" class="logo">
					<span class="logo-lg"><b>동화마루 CMS</b></span>
				</a>
				<nav class="navbar navbar-static-top">
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- User Account: style can be found in dropdown.less -->
							<li class="dropdown user user-menu">
								<a href="/login/logout.php"">
									<span class="hidden-xs">Sign out</span>
								</a>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<div class="content-wrapper">
				<section class="content">
					<div class="row">
						<div class="col-md-3">
							<div class="box box-danger">
								<div class="box-header with-border">
									<h3 class="box-title">Player List</h3>
									<div class="box-tools pull-right">
										<button type="button" id="btnAddPlayer" class="btn btn-info pull-right">추가</button>
									</div>
								</div>
								<div class="box-body table-responsive no-padding player-list-wrapper">
									<table class="table table-hover">
										<tbody id="player_list_tbody">
										<?php 
											foreach ($player_list as $row) {
										?>
											<tr id="<?php echo $row["player_pk"]; ?>">
												<td><b><?php echo $row["player_name"]; ?></b> (<?php echo $row["player_id"]; ?>)</td>
												<td class="width-120">
													<button type="button" class="btn btn-info btn-sm btnRebootPlayer" data-id="<?php echo $row["player_id"]; ?>"><i class="fa fa-repeat"></i></button>
													<button type="button" class="btn btn-warning btn-sm btnDeletePlayer"><i class="fa fa-times"></i></button>
												</td>
											</tr>
										<?php
											}
										?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="box box-info hidden">
								<div class="box-header with-border">
									<h3 class="box-title">미리보기</h3>
									<span style="color: red;">(미리보기는 mp4 파일만 지원됩니다.)</span>
								</div>
								<div class="box-body table-responsive no-padding text-center" style="position: relative; height: 150px;">
									<div id="video_wrapper"></div>
									<div id="popuplayer_wrapper"></div>
								</div>
								<div class="box-footer clearfix text-center">
									<div class="pull-left">
										<button type="button" class="btn btn-warning btn-sm" id="btnPlayPreview" data-playing-mode="pause">미리보기 시작</button>
									</div>
									<div class="pull-right">
										<span id="current_play_time">00:00:00</span> / 
										<span id="duration">00:00:00</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-9">
							<form id="componentForm" method="POST" action="/ajax/ajax.php" enctype="multipart/form-data">
								<input type="hidden" name="selected_player_pk" id="selected_player_pk"/>
								<input type="hidden" name="deleted_bg_component_pk" id="deleted_bg_component_pk"/>
								<input type="hidden" name="deleted_popup_component_pk" id="deleted_popup_component_pk"/>
								<input type="hidden" name="type" value="update_component"/>
								<input type="hidden" name="form_data" id="form_data"/>
								<div class="box box-info">
									<div class="box-body">
										<div class="row">
											<div class="col-md-12">
												<div class="box box-success">
													<div class="box-header with-border">
														<h3 class="box-title">배경 Layer</h3>
														<div class="box-tools pull-right">
															<label id="btnAddBgComponent" class="btn btn-info">추가<input type="file" name="bg_component_file[]" id="bg_component_file" class="hidden" accept="video/*"/></label>
														</div>
													</div>
													<div class="box-body table-responsive no-padding">
														<table class="table table-hover">
															<thead>
																<th class="text-center width-120">순서</th>
																<th class="text-center">파일 명</th>
																<th class="text-center width-80">삭제</th>
																<th class="width-20"></th>
															</thead>
														</table>
														<div class="table-body-wrapper">
															<table class="table table-hover">
																<tbody id="bg_list_tbody"></tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="box box-success">
													<div class="box-header with-border">
														<h3 class="box-title">팝업 Layer</h3>
														<div class="box-tools pull-right">
															<button type="button" id="btnAddClockComponent" class="btn btn-info">시계 추가</button>
															<button type="button" id="btnAddWeatherComponent" class="btn btn-info">날씨 추가</button>
															<button type="button" id="btnAddWebviewComponent" class="btn btn-info">웹뷰 추가</button>
															<label id="btnAddImageComponent" class="btn btn-info">이미지 추가<input type="file" name="image_component_file[]" id="image_component_file" class="hidden" accept="image/*"/></label>
														</div>
													</div>
													<div class="box-body table-responsive no-padding">
														<table class="table table-hover">
															<thead>
																<th class="text-center width-120">구분</th>
																<th class="text-center">파일 명</th>
																<th class="text-center width-120">표시위치</th>
																<th class="text-center play-time">시작 시간</th>
																<th class="text-center play-time">종료 시간</th>
																<th class="text-center width-50">삭제</th>
																<th class="width-20"></th>
															</thead>
														</table>
														<div class="table-body-wrapper">
															<table class="table table-hover">
																<tbody id="popup_list_tbody"></tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="box-footer clearfix">
										<button type="button" id="btnSaveComponent" class="btn btn-info pull-right">적용</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					<b>Version</b> 1.0.0
				</div>
				<strong>Copyright &copy; 2016 <a href="http://www.qvoss.co.kr">QVOSS</a>.</strong> All rights reserved.
			</footer>
		</div>
		<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="modal_popup_label"></h4>
					</div>
					<div class="modal-body" id="modal_popup_content"></div>
					<div class="modal-footer" id="modal_popup_footer"></div>
				</div>
			</div>
		</div>
	</body>
</html>
