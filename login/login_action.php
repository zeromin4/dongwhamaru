<?php
	header("Content-Type: text/html; charset=UTF-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CMysqlManager", "CAccountManager"));
	
		$session = new CSession();
		$mysql_manager = new CMysqlManager();
		$accountManager = new CAccountManager($mysql_manager->getDb());
		
		$id = isset($_POST["id"]) ? $_POST["id"] : "";
		$pw = isset($_POST["password"]) ? $_POST["password"] : "";
		
		if(!empty($id) && !empty($pw)) {
			$pw = md5($pw);
			
			$account_info = $accountManager->login($id, $pw);
			if(!$account_info) {
				echo "<script>";
				echo "alert('로그인 실패.');";
				echo "location.href = '/login/login.php';";
				echo "</script>";
				exit;
			}
			
			$session->login($account_info);
		}

		moveToSpecificPage("/");
		exit;
	} catch (Exception $e) {
		echo "<pre>";
		print_r($e->getMessage());
		exit;
	}
?>
