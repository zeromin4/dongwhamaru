<?php
	/**
	 * require_once_classes(Array("CUtil"));
	 */
	function require_once_classes($_classNameArr) {
		foreach ($_classNameArr AS $className) {
			require_once(CONF_PATH_CLASS . $className . ".php");
		}
	}

	/**
	 * debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, "can not connect to memcached");
	 */
	function debug_mesg($_type, $_class, $_function, $_line, $_mesg) {
		if (!CONF_DEBUG) {
			return;
		}
	
		if ((strcmp($_type, "E") == 0) && !CONF_DEBUG_ERROR) {
			return;
		} else if ((strcmp($_type, "I")) && !CONF_DEBUG_INFO) {
			return;
		}
	
		$_mesg .= "@1-". whereCalled(1);
		$_mesg .= "@2-". whereCalled(2);
		$_mesg .= "@3-". whereCalled(3);
		
		if (CONF_DEBUG_DISPLAY) {
			printf("%s|%s|%s|%s|%d|%s<br/>\r\n", date("Y-m-d H:i:s"), $_type, $_class, $_function, $_line, substr($_mesg, 0, 51200));
		}
	
		if (CONF_DEBUG_SAVE) {
			$save_file = CONF_DEBUG_SAVE_FILE_ETC;
			if ($_type == "E") {
				$save_file = CONF_DEBUG_SAVE_FILE_ERROR;
			}
			
			$f = sprintf("%s%d_%s", CONF_PATH_DEBUG_FILE, date("YmdH"), $save_file);
	
			$is_new = true;
			if (file_exists($f)) {
				$is_new = false;
			}
	
			$fp = fopen($f, "a");
	
			if ($is_new) {
				chmod($f, 0666);
			}
	
			if ($fp) {
				printf("%s|%s|%s|%s|%d|%s\r\n", date("Y-m-d H:i:s"), $_type, $_class, $_function, $_line, substr($_mesg, 0, 51200));
				fclose($fp);
			}
		}
	}

	function whereCalled($_level = 1) {
		$trace = debug_backtrace();
		
		if (!isset($trace[$_level])) {
			return "nothing";
		}
			
		$file = $trace[$_level]["file"];
		$line = $trace[$_level]["line"];
		$object = isset($trace[$_level]["object"]) ? $trace[$_level]["object"] : null;
		
		$args = "";
		if ($trace[$_level]["args"]) {
			unSet($trace[$_level]["args"]["_query"]);
			$args = serialize($trace[$_level]["args"]);
		}
	
		if (is_object($object)) {
			$object = get_class($object);
		}
	
		return "Where called [" . $_level . "]: line " . $line . " of " . $object . " with args[" . $args . "] (in " . $file . ")";
	}

	function snc_return($_result = "OK", $_message = "SUCCESS", $_data = null) {
	    return json_encode(Array("result" => $_result, "message" => $_message, "data" => $_data), JSON_UNESCAPED_UNICODE);
	}
	
	function snc_error($_message) {
		global $_SNC_ERROR;
	
		$_SNC_ERROR = Array('message' => $_message);
	}
	
	function moveToSpecificPage($_url, $_redirect_url = null) {
		if(isset($_redirect_url) && !empty($_redirect_url)) {
			$_url = "?redirect=" . $_redirect_url;
		}
		
		header('Location: ' . filter_var($_url, FILTER_SANITIZE_URL));
	}
	
	function uniqid_base36($more_entropy = true) {
		$s = uniqid('', $more_entropy);
		if (!$more_entropy)
			return base_convert($s, 16, 36);
	
			$hex = substr($s, 0, 13);
			$dec = $s[13] . substr($s, 15); // skip the dot
			return base_convert($hex, 16, 36) . base_convert($dec, 10, 36);
	}
	
	function startsWith($haystack, $needle) {
		$length = strlen($needle);
		return (substr($haystack, 0, $length) === $needle);
	}
	
	function endsWith($haystack, $needle) {
		$length = strlen($needle);
		if ($length == 0) {
			return true;
		}
	
		return (substr($haystack, -$length) === $needle);
	}
?>
