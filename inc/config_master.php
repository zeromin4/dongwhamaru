<?php
	define("CONF_SITE_TITLE", "동화마루 CMS");
	
	/**
	 * URL
	 */	
	define('CONF_URL_AJAX',						CONF_URL_ROOT . "ajax/ajax.php");
	define('CONF_URL_AJAX_ATTACHMENT',			CONF_URL_ROOT . "ajax/uploadAttachmemt.php");
	define('CONF_URL_AJAX_ATTACHMENT_DELETE',	CONF_URL_ROOT . "ajax/deleteAttachment.php");
	
	define('CONF_REBOOT_PLAYER_FILE_PATH',		CONF_PATH_ROOT . "data/reboot_players.json");
?>