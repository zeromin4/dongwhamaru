<?php
	session_start();
	
	date_default_timezone_set('Asia/Seoul');

	/**
	 * DATABASE
	 */
	define("DEF_MYSQL_IP",		"dev.sncinteractive.com");
	define("DEF_MYSQL_PORT",	3306);
	define("DEF_MYSQL_USER",	"dongwhamaru");
	define("DEF_MYSQL_PASS",	"dongwhamaru12#$");
	define("DEF_MYSQL_DB",		"dongwhamaru");
	
	/**
	 * PATH
	 */
	define('CONF_SERVER_HOST',		"http://" . $_SERVER['HTTP_HOST']);
	
	define('CONF_URL_ROOT',			"/");
	define('CONF_PATH_ASSETS', 		CONF_URL_ROOT . "assets/");
	define('CONF_URL_FILE',			CONF_URL_ROOT . "data/upload/");
	
	define("CONF_PATH_ROOT",		$_SERVER["DOCUMENT_ROOT"] . CONF_URL_ROOT);
	define("CONF_PATH_CLASS",		CONF_PATH_ROOT . "inc/classes/");
	define('CONF_PATH_DEBUG_FILE',	CONF_PATH_ROOT . "logs/");
	define('CONF_PATH_FILE',		CONF_PATH_ROOT . "data/upload/");
	
	/**
	 * DEBUG
	 */
	define('CONF_DEBUG',			true);
	define('CONF_DEBUG_INFO',		false);
	define('CONF_DEBUG_ERROR',		true);
	define('CONF_DEBUG_DISPLAY',	false);
	define('CONF_DEBUG_SAVE',		true);
	define('CONF_DEBUG_SAVE_FILE_ERROR',	'debug_mesg_error.log');
	define('CONF_DEBUG_SAVE_FILE_ETC',		'debug_mesg_etc.log');
	
	/**
	 * DEFINE
	 */
	include_once('config_master.php');
	
	/**
	 * GLOBAL FUNCTIONS
	 */
	include_once("global_function.php");
?>