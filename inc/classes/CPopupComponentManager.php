<?php
	class CPopupComponentManager {
		var $mysql;
				
		function CPopupComponentManager($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getPopupComponentList($_player_pk) {
			try {
				$this->mysql->where("player_pk", $_player_pk)->where("yn_delete", "N");
				$component_list = $this->mysql->get("component_popup");
				
				return $component_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get component list; getPopupComponentList(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function insertComponent($_component_list) {
			try {
				foreach ($_component_list as $newData) {
					$action = $newData["action"];
					unset($newData["action"]);
					
					if($action == "insert") {
						$this->mysql->insert("component_popup", $newData);
					} else if($action == "update") {
						$this->mysql->where("component_pk", $newData["component_pk"]);
						$this->mysql->update("component_popup", $newData);
					}
				}
						
				return $newData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert component; insertComponent(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function deleteComponentByPlayerPk($_player_pk) {
			try {
				$updateData = array(
					"yn_delete" => "Y"
				);
				
				$this->mysql->where("player_pk", $_player_pk);
				$ret = $this->mysql->update("component_popup", $updateData);
				if($ret) {
					return $updateData;
				} else {
					return null;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to delete component; deletePlayerByPlayerPk(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function deleteComponentByComponentPks($_component_pk) {
			try {
				$updateData = array(
					"yn_delete" => "Y"
				);
				
				$_component_pk = array_map('intval', explode(',', $_component_pk));
		
				$this->mysql->where("component_pk", $_component_pk, "in");
				$ret = $this->mysql->update("component_popup", $updateData);
				if($ret) {
					return $updateData;
				} else {
					return null;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to delete component; deletePlayerByComponentPk(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
	}
?>