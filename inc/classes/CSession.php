<?php
	class CSession {
		function CSession() {
			
		}
		
		function login($_account_info) {
			if(isset($_account_info) && !empty($_account_info)) {
				$_SESSION['account'] = $_account_info;
			}
		}
		
		function logout() {
			unset($_SESSION['account']);
		}
		
		function getLoginData() {
			if(isset($_SESSION['account'])) {
				return $_SESSION['account'];
			} else {
				return null;
			}
		}
	}
?>