<?php
	class CPlayerManager {
		var $mysql;
		var $BgComponentManager;
		var $PopupComponentManager;
				
		function CPlayerManager($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getBgComponentManagerClass() {
			if ($this->BgComponentManager) {
				return;
			}
		
			require_once_classes(Array('CBgComponentManager'));
			$this->BgComponentManager = new CBgComponentManager($this->mysql);
		}
		
		function getPopupComponentManagerClass() {
			if ($this->PopupComponentManager) {
				return;
			}
		
			require_once_classes(Array('CPopupComponentManager'));
			$this->PopupComponentManager = new CPopupComponentManager($this->mysql);
		}
		
		function getPlayerList() {
			try {
				$this->mysql->where("yn_delete", "N");
				$player_list = $this->mysql->get("player");
				
				return $player_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get player list; getPlayerList(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function getPlayerDetailByPk($_player_pk) {
			try {
				$this->mysql->where("player_pk", $_player_pk)->where("yn_delete", "N");
				$player_detail = $this->mysql->getOne("player");
		
				return $player_detail;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get player detail; getPlayerDetailByPk(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function insertPlayer($_player_id, $_player_name = "") {
			try {
				$newData = array(
					"player_id" => $_player_id,
					"player_name" => $_player_name,
					"yn_delete" => "N",
					"reg_date" => date("Y-m-d H:i:s"),
					"update_date" => date("Y-m-d H:i:s")
				);
				
				$newData['player_pk'] = $this->mysql->insert("player", $newData);
						
				return $newData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert player; insertPlayer(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function updatePlayer($_player_pk, $bg_component_data, $popup_component_data) {
			try {
				$updateData = array(
					"update_date" => date("Y-m-d H:i:s")
				);
				
				$this->mysql->where("player_pk", $_player_pk);
				$ret = $this->mysql->update("player", $updateData);
				
				if($ret) {
					if($bg_component_data != null) {
						$this->getBgComponentManagerClass();
						$this->BgComponentManager->insertComponent($bg_component_data);
					}
					
					if($popup_component_data != null) {
						$this->getPopupComponentManagerClass();
						$this->PopupComponentManager->insertComponent($popup_component_data);
					}
					
					$player_component_list = $this->getComponentListByPlayerPk($_player_pk);
					
					return $player_component_list;
				} else {
					return null;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update player; updatePlayer(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function deletePlayer($_player_pk) {
			try {
				$updateData = array(
					"yn_delete" => "Y"
				);
				
				$this->mysql->where("player_pk", $_player_pk);
				$ret = $this->mysql->update("player", $updateData);
				if($ret) {
					return true;
				} else {
					return false;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to delete player; deletePlayer(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function getComponentListByPlayerPk($_player_pk) {
			try {
				$this->mysql->where("player_pk", $_player_pk)->where("yn_delete", "N");
				$this->mysql->orderby("priority", "ASC");
				$bg_component_list = $this->mysql->get("component_background");
				
				$this->mysql->where("player_pk", $_player_pk)->where("yn_delete", "N");
				$popup_component_list = $this->mysql->get("component_popup");
				
				$player_component_list = array();
				
				foreach ($bg_component_list as $row) {
					$player_component_list[] = $row;
				}
				
				foreach ($popup_component_list as $row) {
					$player_component_list[] = $row;
				}
				
				return $player_component_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get player detail; getPlayerDetailByPk(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function getComponentListForExternal($_player_id, $_last_update_time) {
			try {
				$ret = array(
					"BackgroundContentList" => array(),
					"PopupContentList" => array()
				);
				
				if($_last_update_time != null) {
					$bg_component_list = $this->mysql->rawQuery("SELECT bg.* 
															FROM player AS player JOIN component_background AS bg ON player.player_pk = bg.player_pk
															WHERE player.player_id = '" . $_player_id . "' AND player.update_date > '" . $_last_update_time . "' AND bg.yn_delete = 'N' ORDER BY bg.priority");
			
					$popup_component_list = $this->mysql->rawQuery("SELECT popup.* 
															FROM player AS player JOIN component_popup AS popup ON player.player_pk = popup.player_pk
															WHERE player.player_id = '" . $_player_id . "' AND player.update_date > '" . $_last_update_time . "' AND popup.yn_delete = 'N'");
				} else {
					$bg_component_list = $this->mysql->rawQuery("SELECT bg.* 
															FROM player AS player JOIN component_background AS bg ON player.player_pk = bg.player_pk
															WHERE player.player_id = '" . $_player_id . "' AND bg.yn_delete = 'N' ORDER BY bg.priority");
			
					$popup_component_list = $this->mysql->rawQuery("SELECT popup.* 
															FROM player AS player JOIN component_popup AS popup ON player.player_pk = popup.player_pk
															WHERE player.player_id = '" . $_player_id . "' AND popup.yn_delete = 'N'");
				}
				
				foreach ($bg_component_list as $row) {
					$ret["BackgroundContentList"][] = array(
						"ContSeq" => $row["priority"],
						"ContNM" => $row["component_name"],
						"ContTP" => $row["component_name"],
						"ContUrl" => $row["file_path"],
						"DisplayPanel" => "0",
						"StartTS" => "",
						"EndTS" => ""
					);
				}
		
				foreach ($popup_component_list as $row) {
					$ret["PopupContentList"][] = array(
						"ContSeq" => 0,
						"ContNM" => $row["component_name"],
						"ContTP" => $row["component_name"],
						"ContUrl" => isset($row["file_path"]) ? $row["file_path"] : "",
						"StartTS" => $row["start_time"],
						"DisplayPanel" => $row["display_position"],
						"EndTS" => $row["end_time"]
					);
				}
				
				return $ret;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get player detail; getPlayerDetailByPk(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
	}
?>