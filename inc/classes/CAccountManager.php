<?php
	class CAccountManager {
		var $mysql;
				
		function CAccountManager($_mysql) {
			$this->mysql = $_mysql;
		}
		
		function getAccountList() {
			try {
				$account_list = $this->mysql->get("account");
				
				return $account_list;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get account list; getAccountList(); ERROR[" . $e->getMessage() . "]");
				
				return null;
			}
		}
		
		function getAccountDataById($_account_id) {
			try {
				$this->mysql->where("account_id", $_account_id);
				$account_data = $this->mysql->getOne("account");
		
				return $account_data;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to get account data; getAccountDataById(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function login($_id, $_pw) {
			try {
				$this->mysql->where('account_id', $_id)->where('account_pw', $_pw);
				$account_info = $this->mysql->getOne("account", array("account_pk", "account_id", "name"));
			
				return $account_info;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to login; Login(); ERROR[" . $e->getMessage() . "]");
			
				return null;
			}
		}
		
		function insertAccount($_account_id, $_account_pw, $_name) {
			try {
				$newData = array(
					"account_id" => $_account_id,
					"account_pw" => md5($_account_pw),
					"name" => $_name,
					"reg_date" => date("Y-m-d H:i:s")
				);
				
				$newData['account_pk'] = $this->mysql->insert("account", $newData);
						
				return $newData;
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to insert account; insertAccount(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function updateAccount($_account_pk, $_account_id, $_account_pw, $_name) {
			try {
				$updateData = array(
					"account_id" => $_account_id,
					"name" => $_name,
					"update_date" => date("Y-m-d H:i:s")
				);
				
				if(isset($_account_pw) && !empty($_account_pw)) {
					$updateData["account_pw"] = md5($_account_pw);
				}
		
				$this->mysql->where("account_pk", $_account_pk);
				$ret = $this->mysql->update("account", $updateData);
				if($ret) {
					unset($updateData["account_pw"]);
					$updateData["account_pk"] = $_part_order_account_pk;
					
					return $updateData;
				} else {
					return null;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update account; updateAccount(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
		
		function updateAccountStatus($_account_pk, $_account_status) {
			try {
				$updateData = array(
					"account_status" => $_account_status
				);
				
				$this->mysql->where("account_pk", $_account_pk);
				$ret = $this->mysql->update("account_pk", $updateData);
				if($ret) {
					return $updateData;
				} else {
					return null;
				}
			} catch (Exception $e) {
				debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $e->getMessage());
				throw new Exception("Fail to update account; updateAccountStatus(); ERROR[" . $e->getMessage() . "]");
		
				return null;
			}
		}
	}
?>