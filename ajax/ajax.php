<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CMysqlManager", "CBgComponentManager", "CAccountManager", "CPlayerManager", "CPopupComponentManager"));
		require_once('../inc/classes/getid3/getid3.php');
	
		$session = new CSession();
		$account_info = $session->getLoginData();
		
		if(!isset($account_info)) {
			echo snc_return("NOT_LOGIN", "Not Login");
			exit;
		}
	
		$mysql_manager = new CMysqlManager();
		
		$bg_component_manager = new CBgComponentManager($mysql_manager->getDb());
		$player_manager = new CPlayerManager($mysql_manager->getDb());
		$popup_component_manager = new CPopupComponentManager($mysql_manager->getDb());
		$account_manager = new CAccountManager($mysql_manager->getDb());
		
		$type = $_POST['type'];
		$ret = array("result" => "OK", "message" => "SUCCESS", "data" => null);
		
		if($type == "add_player") {
			$player_id = $_POST["player_id"];
			$player_name = isset($_POST["player_name"]) ? $_POST["player_name"] : "";
			
			$ret["data"] = $player_manager->insertPlayer($player_id, $player_name);
		} else if($type == "delete_player") {
			$player_pk = $_POST["player_pk"];
			
			$deleteRet = $player_manager->deletePlayer($player_pk);
			if(!$deleteRet) {
				$ret["result"] = "ERROR";
				$ret["message"] = "Player 삭제 실패.";
			}
		} else if($type == "reboot_player") {
			$player_pk = $_POST["player_pk"];
			$player_id = $_POST["player_id"];
			
			$player_reboot_lilst = json_decode(file_get_contents(CONF_REBOOT_PLAYER_FILE_PATH), true);
			
			if(!isset($player_reboot_lilst)) {
				$player_reboot_lilst = array();
			}
			
			$player_reboot_lilst[$player_id] = "Y";
			
			file_put_contents(CONF_REBOOT_PLAYER_FILE_PATH, json_encode($player_reboot_lilst, JSON_UNESCAPED_UNICODE));
		} else if($type == "get_player_component_list") {
			$player_pk = $_POST["player_pk"];
			
			$ret["data"] = $player_manager->getComponentListByPlayerPk($player_pk);
		} else if($type == "update_component") {
			try {
				$form_data = isset($_POST["form_data"]) ? $_POST["form_data"] : null;
				if($form_data == null) {
					throw new Exception("업데이트 정보가 없습니다.");
				}
				
				$selected_player_pk = $_POST["selected_player_pk"];
				$deleted_bg_component_pk = isset($_POST["deleted_bg_component_pk"]) ? $_POST["deleted_bg_component_pk"] : null;
				$deleted_popup_component_pk = isset($_POST["deleted_popup_component_pk"]) ? $_POST["deleted_popup_component_pk"] : null;
				$form_data = json_decode($form_data, true);
				
				if(!isset($form_data["bg_component"]) || empty($form_data["bg_component"])) {
					throw new Exception("메인 동영상을 올려주세요.");
				}
				
				if(!file_exists(CONF_PATH_FILE . $selected_player_pk)) {
					mkdir(CONF_PATH_FILE . $selected_player_pk, 0755);
				}
				
				$mysql_manager->getDb()->startTransaction();
				
				if($deleted_bg_component_pk != null) {
					$bg_component_manager->deleteComponentByComponentPks($deleted_bg_component_pk);
				}
					
				if($deleted_popup_component_pk != null) {
					$popup_component_manager->deleteComponentByComponentPks($deleted_popup_component_pk);
				}
				
				$getID3 = new getID3;
				$getID3->setOption(array('encoding' => 'UTF-8'));
					
				$bg_component_data = array();
				$total_duration = 0;
				$i = 0;
				foreach ($form_data["bg_component"] as $row) {
					$bg_component_data[] = array(
						"action" => $row["data_new"] == "new" ? "insert" : "update",
						"player_pk" => $selected_player_pk,
						"component_name" => $row["component_name"],
						"priority" => $row["priority"],
						"yn_delete" => "N",
						"update_date" => date("Y-m-d H:i:s")
					);
					
					if($row["data_new"] == "old") {
						$bg_component_data[$i]["component_pk"] = $row["component_pk"];
						$file_analyze = $getID3->analyze(CONF_PATH_ROOT . $row["file_path"]);
						
						$total_duration += $file_analyze["playtime_seconds"];
					} else if($row["data_new"] == "new" && isset($_FILES['bg_component_file']) && !empty($_FILES['bg_component_file'])) {
						if(!startsWith($_FILES['bg_component_file']['type'][$row["new_file_index"]], "video")) {
							throw new Exception("비디오 파일만 업로드 가능합니다.");
						}
						
						$file_ext = explode(".", basename($_FILES['bg_component_file']['name'][$row["new_file_index"]]));
						$upload_file_path = $selected_player_pk . "/" . uniqid($i) . "." . $file_ext[1];
						
						if (move_uploaded_file($_FILES['bg_component_file']['tmp_name'][$row["new_file_index"]], CONF_PATH_FILE . $upload_file_path)) {
							$file_analyze = $getID3->analyze(CONF_PATH_FILE . $upload_file_path);
							$bg_component_data[$i]["org_file_name"] = $_FILES['bg_component_file']['name'][$row["new_file_index"]];
							$bg_component_data[$i]["file_path"] = CONF_URL_FILE . $upload_file_path;
							$bg_component_data[$i]["reg_date"] = date("Y-m-d H:i:s");
							$bg_component_data[$i]["playtime"] = $file_analyze["playtime_string"];
							
							$total_duration += $file_analyze["playtime_seconds"];
						} else {
							throw new Exception("파일 업로드 중 오류가 발생 하였습니다.");
						}
					}
					
					$i++;
				}
				
				$popup_component_data = array();
				if(isset($form_data["popup_component"]) && !empty($form_data["popup_component"])) {
					$i = 0;
					foreach ($form_data["popup_component"] as $row) {
						$popup_component_data[] = array(
							"action" => $row["data_new"] == "new" ? "insert" : "update",
							"player_pk" => $selected_player_pk,
							"component_name" => $row["component_name"],
							"display_position" => $row["display_position"],
							"start_time" => $row["start_time"],
							"end_time" => $row["end_time"],
							"yn_delete" => "N",
							"update_date" => date("Y-m-d H:i:s")
						);
						
						if(!checkTimeValidation($total_duration, $row["start_time"], $row["end_time"])) {
							throw new Exception("시작/종료 시간을 다시 입력해 주세요.");
						}
						
						if($row["data_new"] == "old") {
							$popup_component_data[$i]["component_pk"] = $row["component_pk"];
						} else if($row["data_new"] == "new" && $row["component_name"] == "image" && isset($_FILES['image_component_file']) && !empty($_FILES['image_component_file'])) {
							if(!startsWith($_FILES['image_component_file']['type'][$row["new_file_index"]], "image")) {
								throw new Exception("이미지 파일만 업로드 가능합니다.");
							}
							
							$file_ext = explode(".", basename($_FILES['image_component_file']['name'][$row["new_file_index"]]));
							$upload_file_path = $selected_player_pk . "/" . uniqid($i) . "." . $file_ext[1];
					
							if (move_uploaded_file($_FILES['image_component_file']['tmp_name'][$row["new_file_index"]], CONF_PATH_FILE . $upload_file_path)) {
								$popup_component_data[$i]["file_path"] = CONF_URL_FILE . $upload_file_path;
								$popup_component_data[$i]["org_file_name"] = $_FILES['image_component_file']['name'][$row["new_file_index"]];
								$popup_component_data[$i]["reg_date"] = date("Y-m-d H:i:s");
							} else {
								throw new Exception("파일 업로드 중 오류가 발생 하였습니다.");
							}
						}
						
						if($row["component_name"] == "webview") {
							$popup_component_data[$i]["file_path"] = $row["url"];
						}
						
						$i++;
					}
				}
				
				$ret["data"] = $player_manager->updatePlayer($selected_player_pk, $bg_component_data, $popup_component_data);
				
				$mysql_manager->getDb()->commit();
			} catch (Exception $e) {
				$mysql_manager->getDb()->rollback();
				echo snc_return("ERROR", "업데이트 중 오류가 발생 하였습니다.[" . $e->getMessage() . "]");
				exit;
			}
		}
	} catch (Exception $e) {
		echo snc_return("ERROR", $e->getMessage());
		exit;
	}
	
	echo snc_return($ret["result"], $ret["message"], $ret["data"]);
	exit;
	
	function checkTimeValidation($_totalDuration, $_startTime, $_endTime) {
		$startTimeArray = explode(":", $_startTime);
		$startTime = (intval($startTimeArray[0]) * 60) + intval($startTimeArray[1]);
		$endTimeArray = explode(":", $_endTime);
		$endTime = (intval($endTimeArray[0]) * 60) + intval($endTimeArray[1]);
		
		if($startTime >= $_totalDuration || $endTime > $_totalDuration) {
			return false;
		}
		
		return true;
	}
?>