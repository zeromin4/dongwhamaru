1. database 생성
	CREATE DATABASE `dongwhamaru` /*!40100 DEFAULT CHARACTER SET utf8 */;
2. 사용자 생성
	GRANT ALL PRIVILEGES ON dongwhamaru.* To 'dongwhamaru'@'%' IDENTIFIED BY 'dongwhamaru';
3. 테이블 생성
	CREATE TABLE `account` (
	  `account_pk` int(11) NOT NULL AUTO_INCREMENT,
	  `account_id` varchar(45) NOT NULL,
	  `account_pw` varchar(45) NOT NULL,
	  `name` varchar(45) DEFAULT NULL,
	  `reg_date` datetime DEFAULT CURRENT_TIMESTAMP,
	  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (`account_pk`)
	) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
	
	INSERT INTO `dongwhamaru`.`account` (`account_id`, `account_pw`, `name`, `reg_date`, `update_date`)
	VALUES ('admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', now(), now());
	
	CREATE TABLE `component_background` (
	  `component_pk` int(11) NOT NULL AUTO_INCREMENT,
	  `player_pk` int(11) DEFAULT NULL,
	  `component_name` varchar(45) DEFAULT NULL,
	  `priority` int(11) DEFAULT NULL,
	  `org_file_name` varchar(1024) DEFAULT NULL,
	  `file_path` varchar(1024) DEFAULT NULL,
	  `playtime` varchar(16) DEFAULT NULL,
	  `yn_delete` char(1) DEFAULT NULL,
	  `reg_date` datetime DEFAULT CURRENT_TIMESTAMP,
	  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (`component_pk`)
	) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
	
	CREATE TABLE `component_popup` (
	  `component_pk` int(11) NOT NULL AUTO_INCREMENT,
	  `player_pk` int(11) DEFAULT NULL,
	  `component_name` varchar(45) DEFAULT NULL,
	  `org_file_name` varchar(1024) DEFAULT NULL,
	  `file_path` varchar(1024) DEFAULT NULL,
	  `start_time` varchar(16) DEFAULT NULL,
	  `end_time` varchar(16) DEFAULT NULL,
	  `yn_delete` char(1) DEFAULT NULL,
	  `reg_date` datetime DEFAULT CURRENT_TIMESTAMP,
	  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (`component_pk`)
	) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

	CREATE TABLE `player` (
	  `player_pk` int(11) NOT NULL AUTO_INCREMENT,
	  `player_id` varchar(45) NOT NULL,
	  `player_name` varchar(45) NOT NULL,
	  `yn_delete` char(1) DEFAULT NULL,
	  `reg_date` datetime DEFAULT CURRENT_TIMESTAMP,
	  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (`player_pk`)
	) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

