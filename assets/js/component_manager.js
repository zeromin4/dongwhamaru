function drawComponentList(_data) {
	resetComponentList();
	
	$.each(_data, function(key, val) {
		var trId = "old_" + val.component_name + "_" + val.component_pk;
		
		val.new_type = "old";
		if(val.component_name == "video") {
			addBgComponentHtml(trId, val);
		} else if(val.component_name == "image") {
			addImageComponentHtml(trId, val);
		} else if(val.component_name == "webview") {
			addWebviewComponentHtml(trId, val);
		} else {
			addComponentHtml(trId, val);
		}
	});
	
	updatePriorityButtons();
}

function resetComponentList() {
	$("#bg_list_tbody").empty();
	$("#popup_list_tbody").empty();
}

function initBackgroundComponentDeleteButtonEvent() {
	$(".btnDeleteBackgroundComponent").off("click").on("click", function(event) {
		var tr_id = $(this).parent("td").parent("tr").attr("id");
		$(this).parent("td").parent("tr").remove();
		
		if(tr_id.startsWith("old")) {
			var id_arr = tr_id.split("_");
			deleted_bg_component_pk.push(id_arr[2]);
		}
		
		updatePriorityButtons();
	});
	
	$("#bg_component_file").off("change").on("change", function(event) {
		var selectedFiles = event.target.files;
		if(selectedFiles.length <= 0) {
			return;
		}
		
		var trId = "new_" + generatePlayerId();
		
		addBgComponentHtml(trId, { org_file_name : selectedFiles[0].name, component_name: "video", new_type : "new" });
	});
	
	$(".btnChangePriorityUp").off("click").on("click", function(event) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		
		var tr_parent = $(this).parent("td").parent("tr");
		var tr_pre_parent = $(tr_parent).prev();
		if(tr_pre_parent) {
			$(tr_pre_parent).before(tr_parent);
		}
		
		updatePriorityButtons();
	});
	
	$(".btnChangePriorityDown").off("click").on("click", function(event) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		
		var tr_parent = $(this).parent("td").parent("tr");
		var tr_next_parent = $(tr_parent).next();
		if(tr_next_parent) {
			$(tr_parent).before(tr_next_parent);
		}
		
		updatePriorityButtons();
	});
	
	updatePriorityButtons();
}

function updatePriorityButtons() {
	$("#bg_list_tbody > tr").each(function() {
		if($(this).is('#bg_list_tbody > tr:first-child')) {
			$(this).children("td:first-child").children(".btnChangePriorityUp").addClass("hidden");
			$(this).children("td:first-child").children(".btnChangePriorityDown").removeClass("hidden");
		} else if($(this).is('#bg_list_tbody > tr:last-child')) {
			$(this).children("td:first-child").children(".btnChangePriorityUp").removeClass("hidden");
			$(this).children("td:first-child").children(".btnChangePriorityDown").addClass("hidden");
		} else {
			$(this).children("td:first-child").children(".btnChangePriorityUp").removeClass("hidden");
			$(this).children("td:first-child").children(".btnChangePriorityDown").removeClass("hidden");
		}
	});
}

function initPopupComponentDeleteButtonEvent() {
	$(".btnDeletePopupComponent").off("click").on("click", function() {
		var tr_id = $(this).parent("td").parent("tr").attr("id");
		$(this).parent("td").parent("tr").remove();
		
		if(tr_id.startsWith("old")) {
			var id_arr = tr_id.split("_");
			deleted_popup_component_pk.push(id_arr[2]);
		}
	});
	
	$("#image_component_file").off("change").on("change", function(event) {
		var selectedFiles = event.target.files;
		if(selectedFiles.length <= 0) {
			return;
		}
		
		var trId = "new_" + generatePlayerId();
		addImageComponentHtml(trId, { org_file_name : selectedFiles[0].name, component_name: "image", new_type : "new", start_time: "00:00", end_time: "00:00" });
	});
}

function addComponentHtml(_tr_id, _data) {
	var html = "";
	
	html += "<tr id='" + _tr_id + "' data-new='" + _data.new_type + "' data-component-type='" + _data.component_name + "'>";
	
	if(_data.component_name == "weather") {
		html += "	<td class='text-center width-120'>날씨</td>";
	} else if(_data.component_name == "clock") {
		html += "	<td class='text-center width-120'>시계</td>";
	} else if(_data.component_name == "webview") {
		
	}
	
	html += "	<td></td>";
	html += "	<td class='text-center width-120'>전체</td>";
	html += "	<td class='text-center play-time'><input type='text' class='form-control' value='" + _data.start_time + "'/></td>";
	html += "	<td class='text-center play-time'><input type='text' class='form-control' value='" + _data.end_time + "'/></td>";
	html += "	<td class='text-center width-50'>";
	html += "		<button type='button' class='btn btn-warning btn-sm btnDeletePopupComponent'><i class='fa fa-times'></i></button>";
	html += "	</td>";
	html += "</tr>";

	$("#popup_list_tbody").append(html);
	
	initPopupComponentDeleteButtonEvent();
}

function addWebviewComponentHtml(_tr_id, _data) {
	var html = "";
	
	html += "<tr id='" + _tr_id + "' data-new='" + _data.new_type + "' data-component-type='" + _data.component_name + "'>";
	html += "	<td class='text-center width-120'>웹뷰</td>";
	html += "	<td><input type='text' class='form-control' placeholder='http://example.com' value='" + _data.file_path + "'/></td>";
	html += "	<td class='text-center width-120'>";
	html += "		<select id='" + _tr_id + "_display_position' class='form-control width-80' style='margin: 0px auto;'>";
	html += "			<option value='1'>1</optiona>";
	html += "			<option value='2'>2</optiona>";
	html += "			<option value='3'>3</optiona>";
	html += "		</select>";
	html += "	</td>";
	html += "	<td class='text-center play-time'><input type='text' class='form-control' value='" + _data.start_time + "'/></td>";
	html += "	<td class='text-center play-time'><input type='text' class='form-control' value='" + _data.end_time + "'/></td>";
	html += "	<td class='text-center width-50'>";
	html += "		<button type='button' class='btn btn-warning btn-sm btnDeletePopupComponent'><i class='fa fa-times'></i></button>";
	html += "	</td>";
	html += "</tr>";

	$("#popup_list_tbody").append(html);
	
	$("#" + _tr_id + "_display_position").val(_data.display_position);
	
	initPopupComponentDeleteButtonEvent();
}

function addImageComponentHtml(_tr_id, _data) {
	var html = "";
	
	html += "<tr id='" + _tr_id + "' data-new='" + _data.new_type + "' data-component-type='" + _data.component_name + "' data-file-path='" + (_data.file_path ? _data.file_path : "") + "'>";
	html += "	<td class='text-center width-120'>이미지</td>";
	html += "	<td>" + _data.org_file_name + "</td>";
	html += "	<td class='text-center width-120'>전체</td>";
	html += "	<td class='text-center play-time'><input type='text' class='form-control' value='" + _data.start_time + "'/></td>";
	html += "	<td class='text-center play-time'><input type='text' class='form-control' value='" + _data.end_time + "'/></td>";
	html += "	<td class='text-center width-50'>";
	html += "		<button type='button' class='btn btn-warning btn-sm btnDeletePopupComponent'><i class='fa fa-times'></i></button>";
	html += "	</td>";
	html += "</tr>";
	
	$("#popup_list_tbody").append(html);
	
	if(_data.new_type == "new") {
		$("#" + _tr_id).append($("#btnAddImageComponent > #image_component_file"));
		$("#btnAddImageComponent").append($("<input type='file' name='image_component_file[]' id='image_component_file' class='hidden' accept='image/*'/>"));
	}
	
	initPopupComponentDeleteButtonEvent();
}

function addBgComponentHtml(_tr_id, _data) {
	var html = "";
	
	html += "<tr id='" + _tr_id + "' data-new='" + _data.new_type + "' data-component-type='" + _data.component_name + "' data-file-path='" + (_data.file_path ? _data.file_path : "") + "'>";
	html += "	<td class='text-center width-120'>";
	html += "		<button class='btn btn-sm btn-default btnChangePriorityUp'><i class='fa fa-arrow-up'></i></button>";
	html += "		<button class='btn btn-sm btn-default btnChangePriorityDown'><i class='fa fa-arrow-down'></i></button>";
	html += "	</td>";
	html += "	<td>" + _data.org_file_name + (_data.playtime ? (" (" + _data.playtime + ")") : "") + "</td>";
	html += "	<td class='text-center width-80'>";
	html += "		<button type='button' class='btn btn-warning btn-sm btnDeleteBackgroundComponent'><i class='fa fa-times'></i></button>";
	html += "	</td>";
	html += "</tr>";
	
	$("#bg_list_tbody").append(html);
	
	if(_data.new_type == "new") {
		$("#" + _tr_id).append($("#btnAddBgComponent > #bg_component_file"));
		$("#btnAddBgComponent").append($("<input type='file' name='bg_component_file[]' id='bg_component_file' class='hidden' accept='video/*'/>"));
	}
	
	initBackgroundComponentDeleteButtonEvent();
}
