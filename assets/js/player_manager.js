var total_duration = 0;
var total_played_time = 0;

function initPlayerListEvent() {
	$(".btnDeletePlayer").off("click").on("click", function(event) {
		var ret = confirm("Player를 삭제하시겠습니까?");
		if(ret) {
			var player_pk = $(this).parent("td").parent("tr").attr("id");
			deletePlayer(player_pk);
		}
	});
	
	$(".btnRebootPlayer").off("click").on("click", function(event) {
		var ret = confirm("Player가 재시작 됩니다. \n계속 진행 하시겠습니까?");
		if(ret) {
			var player_pk = $(this).parent("td").parent("tr").attr("id");
			var player_id = $(this).attr("data-id");
			rebootPlayer(player_pk, player_id);
		}
	});
	
	$('#player_list_tbody').off("click").on('click', 'tr', function (event) {
		var index = $(this).index("#player_list_tbody > tr");
		
		if($(event.target).is('#player_list_tbody > tr:eq(' + index + ') > td:eq(0)')) {
			var selected_player_pk = $(this).attr("id");
			$("#selected_player_pk").val(selected_player_pk)
			
			getPlayerComponentList(selected_player_pk);
			
			$("#player_list_tbody > tr").each(function() {
				if($(this).hasClass("selected")) {
					$(this).removeClass("selected");
				}
			});
			
			$(this).addClass("selected");
		}
	});
}

function showPlayerAddPopup() {
	$("#modal_popup_label").text("Player 추가");
	$("#modal_popup_content").html(getPlayerAddPopupHtml());
	$("#modal_popup_footer").html(getPlayerAddPopupFooterHtml());
	
	$("#player_id").val(generatePlayerId());
	
	$("#modal_popup").modal('show');
	
	$("#btnApplyPlayer").off("click").on("click", function() {
		addPlayer();
	});
	
	setTimeout(function() {
		$("#player_name").focus();
	}, 500);
}

function getPlayerAddPopupHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='player_id'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Player ID</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='player_id' readOnly/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='player_name'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>Player 이름</label>";
	html += "			<div class='col-sm-8'><input type='text' class='form-control' id='player_name'/></div>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function getPlayerAddPopupFooterHtml() {
	var footerHtml = "";
	
	footerHtml += "<button type='button' id='btnApplyPlayer' class='btn btn-primary'>적용</button>";
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	
	return footerHtml;
}

function addPlayer() {
	var params = {};
	params.type = "add_player";
	params.player_id = $.trim($("#player_id").val());
	params.player_name = $.trim($("#player_name").val());
	
	if(params.player_id.length <= 0) {
		alert("Player ID를 입력해 주세요.");
		$("#player_id").focus();
		return;
	}
	
	if(params.player_name.length <= 0) {
		alert("Player 이름을 입력해 주세요.");
		$("#player_name").focus();
		return;
	}
	
	$('.loading').show();
	
	$.post("/ajax/ajax.php", params, function(_data) {
		$('.loading').hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_data);
		
		if(ret_data.result == "OK") {
			addPlayerList(ret_data.data);
			alert("추가 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = "/login/login.php";
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function deletePlayer(_player_pk) {
	var params = {};
	params.type = "delete_player";
	params.player_pk = _player_pk;
	
	$('.loading').show();
	
	$.post("/ajax/ajax.php", params, function(_data) {
		$('.loading').hide();
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			var selected_player_pk = $.trim($("#selected_player_pk").val());
			if(selected_player_pk == _player_pk) {
				$("#selected_player_pk").val("");
				resetComponentList();
			}
			
			$("#" + _player_pk).remove();
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = "/login/login.php";
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function rebootPlayer(_player_pk, _player_id) {
	var params = {};
	params.type = "reboot_player";
	params.player_pk = _player_pk;
	params.player_id = _player_id;
	
	$('.loading').show();
	
	$.post("/ajax/ajax.php", params, function(_data) {
		$('.loading').hide();
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			alert("Player가 재시작 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = "/login/login.php";
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function addPlayerList(_data) {
	if(!_data) {
		return;
	}
	
	var html = "";
	
	html += "<tr id='" + _data.player_pk + "'>";
	html += "	<td><b>" + _data.player_name + "</b> (" + _data.player_id + ")</td>";
	html += "	<td class='width-50'>";
	html += "		<button type='button' class='btn btn-info btn-sm btnRebootPlayer' data-id='" + _data.player_id + "'><i class='fa fa-repeat'></i></button>";
	html += "		<button type='button' class='btn btn-warning btn-sm btnDeletePlayer'><i class='fa fa-times'></i></button>";
	html += "	</td>";
	html += "</tr>";
	
	$("#player_list_tbody").append(html);
	
	initPlayerListEvent();
}

function getPlayerComponentList(_player_pk) {
	var params = {};
	params.type = "get_player_component_list";
	params.player_pk = _player_pk;
	
	$('.loading').show();
	
	clearPreview();
	resetComponentList();
	
	$.post("/ajax/ajax.php", params, function(_data) {
		$('.loading').hide();
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			drawComponentList(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN") {
			location.href = "/login/login.php";
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function getPreviewVideo() {
	var ret = [];
	
	$("#bg_list_tbody > tr").each(function(key, obj) {
		var data_new = $(this).attr("data-new");
		var file_path = "";
		if(data_new == "new") {
			var input = $("#bg_list_tbody > tr > input");
			file_path = URL.createObjectURL(input[0].files[0]);
		} else {
			file_path = $(this).attr("data-file-path");
		}
		
		var html = "";
		html += "<video id='preview_video_" + key + "' src='" + file_path + "' class='preview_video " + (key > 0 ? "hidden" : "") + "' preload='auto' height='150'></video>";
		
		ret.push({component_name: "video", html : html});
	});
	
	$("#popup_list_tbody > tr").each(function(key, obj) {
		var data_new = $(this).attr("data-new");
		var start_time = $(this).children("td:eq(2)").children("input").val();
		var end_time = $(this).children("td:eq(3)").children("input").val();
		var component_name = $(this).attr("data-component-type");
		var file_path = "";
		
		if(component_name == "image" && data_new == "new") {
			var input = $("#popup_list_tbody > tr > input");
			file_path = URL.createObjectURL(input[0].files[0]);
		} else {
			file_path = $(this).attr("data-file-path");
		}
		
		var html = "";
		
		html += "<div class='popup_layer_item hidden' data-start-time='" + start_time + "' data-end-time='" + end_time + "'>";
		if(component_name == "image") {
			html += "<img src='" + file_path + "' style='height: 150px; opacity: 0.7;'/>";
		} else if(component_name == "clock") {
			html += "<span>시계</span>";
		} else if(component_name == "weather") {
			html += "<span>날씨</span>";
		}
		
		html += "</div>";
		
		ret.push({component_name: component_name, html : html});
	});
	
	return ret;
}

function drawPreviewVideo() {
	var data = getPreviewVideo();
	
	clearPreview();
	
	var index = 0;
	for(var i = 0; i < data.length; i++) {
		var val = data[i];
		if(val.component_name == "video") {
			$("#video_wrapper").append(val.html);
			
			var vid = document.getElementById("preview_video_" + i);
			
			vid.onloadedmetadata = function(_event) {
			    total_duration += _event.target.duration;
			    $("#duration").text(calculateTimeToString(total_duration));
			};
			
			vid.ontimeupdate = function(_event) {
				var nowPlayTime = total_played_time + _event.target.currentTime;
			    $("#current_play_time").text(calculateTimeToString(nowPlayTime));
			    
			    showPopupLayer(nowPlayTime);
			};
			
			vid.onpause = function() {
				total_played_time = 0;
				$("#current_play_time").text(calculateTimeToString(total_played_time));
			};
			
			vid.onended = function(_event) {
				total_played_time += _event.target.duration;
				var nextVideo = $(_event.target).next("video");

				if(nextVideo.length > 0) {
					$(_event.target).addClass("hidden");
					$(nextVideo).removeClass("hidden");
					
					var vid = document.getElementById(getNowPlayingId());
					vid.play();
				} else {
					total_played_time = 0;
				}
			};
		} else {
			$("#popuplayer_wrapper").append(val.html);
		}
	}
}

function showPopupLayer(_time) {
	var timeString = calculateTimeToString(_time);
	var timeArray = timeString.split(":");
	$("#popuplayer_wrapper > .popup_layer_item").each(function() {
		var startTime = $(this).attr("data-start-time").split(":");
		var endTime = $(this).attr("data-end-time").split(":");
		
		if(timeArray[1] == startTime[0] && timeArray[2] == startTime[1]) {
			$(this).removeClass("hidden");
		}
		
		if(timeArray[1] == endTime[0] && timeArray[2] == endTime[1]) {
			$(this).addClass("hidden");
		}
	});
}

function getNowPlayingId() {
	var nowPlayingId = "";
	$("#video_wrapper > video").each(function() {
		if(!$(this).hasClass("hidden")) {
			nowPlayingId = $(this).attr("id");
			return;
		}
	});
	
	return nowPlayingId;
}

function calculateTimeToString(_data) {
	var retData = {
		hours: 0,
		minutes: 0,
		seconds: 0
	};
	
	retData.seconds = parseInt(_data % 60);
	
	retData.minutes = _data / 60;
	if(retData.minutes >= 60) {
		retData.hours = parseInt(retData.minutes / 60);
		retData.minutes = parseInt(retData.minutes % 60);
	} else {
		retData.minutes = parseInt(_data / 60);	
	}
	
	if(retData.hours < 10) {
		retData.hours = '0' + retData.hours;
	}
	
	if(retData.minutes < 10) {
		retData.minutes = '0' + retData.minutes;
	}
	
	if(retData.seconds < 10) {
		retData.seconds = '0' + retData.seconds;
	}
	
	return retData.hours + ":" + retData.minutes + ":" + retData.seconds;
}
