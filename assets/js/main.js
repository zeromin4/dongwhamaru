var deleted_popup_component_pk = [];
var deleted_bg_component_pk = [];

$(document).ready(function() {
	initPlayerListEvent();
	
	initBackgroundComponentDeleteButtonEvent();
	
	initPopupComponentDeleteButtonEvent();
	
	$("#btnPlayPreview").off("click").on("click", function() {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		
		var play_mode = $(this).attr("data-playing-mode");
		
		if(play_mode == "pause") {
			drawPreviewVideo();
			
			var vid = document.getElementById(getNowPlayingId());
			if(vid != null) {
				vid.play();
				$(this).text("미리보기 중지");
				$(this).attr("data-playing-mode", "playing");
			}
		} else if(play_mode == "playing") {
			var vid = document.getElementById(getNowPlayingId());
			if(vid != null) {
				vid.pause();
				$(this).text("미리보기 시작");
				$(this).attr("data-playing-mode", "pause");
				clearPreview();
			}
		}
	});
	
	$("#btnAddPlayer").off("click").on("click", function() {
		showPlayerAddPopup();
	});
	
	$("#btnAddBgComponent").off("click").on("click", function(event) {
		checkPlayerSelect();
	});
	
	$("#btnAddImageComponent").off("click").on("click", function(event) {
		checkPlayerSelect();
	});
	
	$("#btnAddClockComponent").off("click").on("click", function() {
		if(!checkPlayerSelect()) {
			return;
		}
		
		var trId = "new_" + generatePlayerId();
		addComponentHtml(trId, { component_name: "clock", new_type : "new", start_time: "00:00", end_time: "00:00" });
	});
	
	$("#btnAddWeatherComponent").off("click").on("click", function() {
		if(!checkPlayerSelect()) {
			return;
		}
		
		var trId = "new_" + generatePlayerId();
		addComponentHtml(trId, { component_name: "weather", new_type : "new", start_time: "00:00", end_time: "00:00" });
	});
	
	$("#btnAddWebviewComponent").off("click").on("click", function() {
		if(!checkPlayerSelect()) {
			return;
		}
		
		var trId = "new_" + generatePlayerId();
		addWebviewComponentHtml(trId, { component_name: "webview", new_type : "new", start_time: "00:00", end_time: "00:00", display_position: 1, file_path: "" });
	});
	
	$("#btnSaveComponent").off("click").on("click", function(event) {
		if(!checkPlayerSelect()) {
			return;
		}
		
		$("#deleted_popup_component_pk").val(deleted_popup_component_pk.join(','));
		$("#deleted_bg_component_pk").val(deleted_bg_component_pk.join(','));
		
		var formData = getFormData();
		if(formData) {
			$("#form_data").val(JSON.stringify(formData));
			
			$('#componentForm').submit();
		}
	});
	
	$('#componentForm').submit(function() {
		$(this).ajaxSubmit({
			beforeSubmit: function(formData, jqForm, options) {
				$('.loading').show();
				
				return true;
			},
			success : function(responseText, statusText, xhr, $form) {
				$('.loading').hide();
				
				$("#deleted_popup_component_pk").val("");
				$("#deleted_bg_component_pk").val("");
				
				var ret_data = JSON.parse(responseText);
				if(ret_data.result == "OK") {
					drawComponentList(ret_data.data);
					alert("적용되었습니다.");
				} else if(ret_data.result == "NOT_LOGIN") {
					location.href = "/login/login.php";
				} else {
					alert(ret_data.message);
				}
			}
		});
		
		return false;
	});
});

function clearPreview() {
	total_duration = 0;
	total_played_time = 0;

	$("#video_wrapper").empty();
	$("#popuplayer_wrapper").empty();
	$("#current_play_time").text("00:00:00");
	$("#duration").text("00:00:00");
}

function getFormData() {
	var formData = {};
	formData.bg_component = [];
	formData.popup_component = [];
	
	var isValid = true;
	
	var bg_priority = 1;
	var new_file_index = 0;
	$("#bg_list_tbody > tr").each(function() {
		var data = {
			priority: bg_priority,
			data_new : $(this).attr("data-new"),
			component_name : $(this).attr("data-component-type"),
			org_file_name : $(this).children("td:eq(1)").text(),
			file_path: $(this).attr("data-file-path"),
			component_pk: 0,
			new_file_index : -1
		};
		
		if(data.data_new == "old") {
			var ids = $(this).attr("id").split("_");
			data.component_pk = ids[2];
		} else {
			data.new_file_index = new_file_index;
			new_file_index++;
		}
		
		formData.bg_component.push(data);
		
		bg_priority++;
	});
	
	if(formData.bg_component.length <= 0) {
		alert("메인 동영상을 올려주세요.");
		return null;
	}
	
	new_file_index = 0;
	var last_end_time = null;
	$("#popup_list_tbody > tr").each(function() {
		var data = {
			data_new : $(this).attr("data-new"),
			component_name : $(this).attr("data-component-type"),
			org_file_name : "",
			url : "",
			display_position : 0,
			start_time: $.trim($(this).children("td:eq(3)").children("input").val()),
			end_time : $.trim($(this).children("td:eq(4)").children("input").val()),
			component_pk: 0,
			new_file_index : -1
		};
				
		if(data.data_new == "old") {
			var ids = $(this).attr("id").split("_");
			data.component_pk = ids[2];
		} else if(data.data_new == "new" && data.component_name == "image") {
			data.new_file_index = new_file_index;
			new_file_index++;
		}
		
		if(data.component_name == "webview") {
			data.display_position = $.trim($(this).children("td:eq(2)").children("select").val());
			data.url = $.trim($(this).children("td:eq(1)").children("input").val());
			
			var pattern = /^(file|gopher|news|nntp|telnet|https?|ftps?|sftp):\/\/([a-z0-9-]+\.)+[a-z0-9]{2,4}.*$/;

			if(data.url.length <= 0 || !pattern.test(data.url)) {
				isValid = false;
				alert("웹뷰 URL을 다시 입력해 주세요.");
				$(this).children("td:eq(1)").children("input").focus();
				return false;
			}
		} else {
			data.org_file_name = $.trim($(this).children("td:eq(1)").text());
		}
		
		if(!checkTimeFormatValidation(data.start_time)) {
			isValid = false;
			alert("시작 시간을 다시 입력해 주세요.");
			$(this).children("td:eq(3)").children("input").focus();
			return false;
		}
		
		if(!checkTimeFormatValidation(data.end_time)) {
			isValid = false;
			alert("종료 시간을 다시 입력해 주세요.");
			$(this).children("td:eq(4)").children("input").focus();
			return false;
		}
		
		if(!checkTimeValidation(data.start_time, data.end_time)) {
			isValid = false;
			alert("시간을 다시 입력해 주세요.");
			$(this).children("td:eq(3)").children("input").focus();
			return false;
		}
		
		if(last_end_time) {
			if(!checkTimeDuplicationValidation(last_end_time, data.start_time)) {
				isValid = false;
				alert("시간을 다시 입력해 주세요.");
				$(this).children("td:eq(3)").children("input").focus();
				return false;
			}
		}
		
		last_end_time = data.end_time;
		
		formData.popup_component.push(data);
	});
	
	if(isValid) {
		return formData;
	} else {
		return null;
	}	
}

function checkTimeFormatValidation(_time) {
	var pattern = /^[\d]{2}:[\d]{2}$/g;
	
	if(!pattern.test(_time)) {
		return false;
	}
	
	var timeArr = _time.split(":");
	if(parseInt(timeArr[1]) >= 60) {
		return false;
	}
	
	return true;
}

function checkTimeValidation(_start_time, _end_time) {
	if(_start_time == _end_time) {
		return false;
	}
	
	if(_start_time >= _end_time) {
		return false;
	}
	
	return true;
}

function checkTimeDuplicationValidation(_start_time, _end_time) {
	if(_start_time > _end_time) {
		return false;
	}
	
	return true;
}

function generatePlayerId() {
	return md5(getUUID());
}

function getUUID() {
	return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

function checkPlayerSelect() {
	var selected_player_pk = $.trim($("#selected_player_pk").val());
	if(selected_player_pk.length <= 0) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		alert("Player를 먼저 선택해 주세요.");
		return false;
	}
	
	return true;
}
