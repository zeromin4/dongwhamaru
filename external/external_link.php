<?php 
	header("Content-Type:text/html; charset=utf-8");

	$ret = array("ResultCD" => "000", "LastTS" => date("Y-m-d H:i:s"), "ResultDatas" => null);
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CMysqlManager", "CPlayerManager"));
		
		if(!isset($_GET["PlayerId"]) || empty($_GET["PlayerId"])) {
			echo snc_return("ERROR", "파라미터 오류!");
			exit;
		}
		
		$player_id = $_GET["PlayerId"];
		$last_update_time = isset($_GET["LastTS"]) ? $_GET["LastTS"] : null;		
		
		$mysql_manager = new CMysqlManager();
		
		$player_manager = new CPlayerManager($mysql_manager->getDb());
			
		$ret["ResultDatas"] = $player_manager->getComponentListForExternal($player_id, $last_update_time);
		
		$player_reboot_lilst = json_decode(file_get_contents(CONF_REBOOT_PLAYER_FILE_PATH), true);
		
		$ret["RebootCD"] = "N";
		if(isset($player_reboot_lilst) && isset($player_reboot_lilst[$player_id]) && $player_reboot_lilst[$player_id] == "Y") {
			$ret["RebootCD"] = "Y";
			unset($player_reboot_lilst[$player_id]);
			
			file_put_contents(CONF_REBOOT_PLAYER_FILE_PATH, json_encode($player_reboot_lilst, JSON_UNESCAPED_UNICODE));
		}
		
		$ret["LastTS"] = date("Y-m-d H:i:s");
	} catch (Exception $e) {
		$ret["ResultCD"] = "100";
		$ret["ResultDatas"] = null;
		$ret["LastTS"] = date("Y-m-d H:i:s");
	}
	
	echo json_encode($ret, JSON_UNESCAPED_UNICODE);

	exit;
?>